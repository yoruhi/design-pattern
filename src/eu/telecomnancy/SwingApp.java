package eu.telecomnancy;

import eu.telecomnancy.sensor.AdapterLegacyTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.Proxy;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        ISensor sensor = new AdapterLegacyTemperatureSensor();
        new MainWindow(sensor);
    }

}
