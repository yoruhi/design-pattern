package eu.telecomnancy.sensor;

import java.util.Random;

public class AdapterLegacyTemperatureSensor extends Observable implements ISensor {
	private LegacyTemperatureSensor l=new LegacyTemperatureSensor();
	//boolean state=l.getStatus();
	double value=l.getTemperature();
	State state;

	@Override
	public void on() {
		//state=true;
		state=new StateSensorOn();
	}
	
	@Override
	public void off() {
		//state=false;
		state=new StateSensorOff();
	}

	@Override
	public boolean getStatus() {
		return false;
		
	}

	@Override
	public void update() throws SensorNotActivatedException {
			state.update();		
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return state.getValue();
	}

}
