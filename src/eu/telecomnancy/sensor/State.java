package eu.telecomnancy.sensor;

public interface State extends ISensor {
	void on();
	void off();
	void update() throws SensorNotActivatedException;
	boolean getStatus();

}
