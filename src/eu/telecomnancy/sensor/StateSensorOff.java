package eu.telecomnancy.sensor;

public class StateSensorOff implements State {
	
		boolean state = false;

		public StateSensorOff() {
			super();
			this.state = false;
		}

		@Override
		public void on(){
			
		}

		@Override
		public void off() {
			state=false;
			
		}

		@Override
		public void update() throws SensorNotActivatedException{
			throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
			
		}

		@Override
		public boolean getStatus() {
			// TODO Auto-generated method stub
			return state;
		}

		@Override
		public double getValue() throws SensorNotActivatedException {
			// TODO Auto-generated method stub
			return 0;
		}

	
}
