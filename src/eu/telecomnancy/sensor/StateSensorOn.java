package eu.telecomnancy.sensor;

import java.util.Random;

public class StateSensorOn implements State{
	boolean state = false;
	double value=0;

	public StateSensorOn() {
		super();
	}

	@Override
	public void on() {
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		value = (new Random()).nextDouble() * 100;	
		
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		return value;
	}
}
