package eu.telecomnancy.sensor;

import java.util.Random;

public class TemperatureSensor extends Observable implements ISensor {
    //boolean state;
    double value = 0;
    State state;

    @Override
    public void on() {
        //state = true;
        state=new StateSensorOn();
    }

    @Override
    public void off() {
        //state = false;
        state=new StateSensorOff();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (state.getStatus())
            value = (new Random()).nextDouble() * 100;
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (state.getStatus())
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

}
